class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  #student requests to enroll in a new course and add to his course list
  def enroll(new_course)
    conflicts = @courses.select {|course| course.conflicts_with?(new_course)}
    if conflicts.empty?
      @courses << new_course unless @courses.include?(new_course)
      new_course.students << self
    else
      raise "error"
    end
  end

  def course_load
    course_load = Hash.new(0)
    @courses.each do |course|
      course_load[course.department] += course.credits
    end
    course_load
  end

end
